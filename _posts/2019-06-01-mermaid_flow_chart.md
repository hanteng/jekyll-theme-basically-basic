---
title: "markdown：使用 mermaid js 实现流程图"
excerpt_separator: "<!--more-->"
categories:
  - 课程
tags:
  - markdown
  - 可视化
---

使用 mermaid js 实现流程图

<!--more-->

<script src='https://unpkg.com/mermaid@7.1.2/dist/mermaid.min.js'></script>
<script>mermaid.initialize({startOnLoad:true});</script>


## 专业核心课
专业核心课为必修：以三门基础概论课开始，在建基的《<span  style="background-color:red;">网页设计与制作</span>》与《Python语言》的两门核心课打下坚实文理多学科基础，开启分析（如《新媒体分析和用户体验》丶《大数据统计》丶《大数据统计》）丶交互应用（如《互动技术与应用》丶《媒介融合/全媒体信息编辑》丶《信息可视化设计》丶《数字音视频编辑》等）两扇专业之门。

<style>
#Web > g > g > * {
    color: white;
    font-weight: bolder;
    font-family: sans-serif;
    word-wrap: break-word;
    white-space: pre-wrap !important;
}
#Web > rect {
	fill: red !important;
}

</style>
<div class="mermaid" style="background-color:lightgreen;">
graph LR;
Tech[网络新媒体技术与协作] --> Web[网页设计与制作]
Tech --> Python[Python语言]
Tech --> InfoVis[信息可视化设计]
Web --> UX[新媒体分析和用户体验]
Web --> BigData[大数据统计]
Web --> HCI[互动技术与应用]
Web --> InfoVis
Web --> Convergence[媒介融合/全媒体信息编辑]
Web --> 数字音视频编辑
Theory[传播学概论] --> Web
Theory --> Convergence
Theory --> BigData
Python --> InfoVis
Python --> BigData
Python --> UX
Intro[新媒体概论] --> Convergence
Intro --> HCI
Intro --> UX
Intro --> Web

</div>


举例来说，课程《网页设计与制作》的特色是以「手机与桌面兼顾」的响应式设计出发，介绍了介面设计（Interface Design）丶图形设计（Graphic Design）的基础知识点，以开启进阶课程《新媒体分析和用户体验》丶《互动技术与应用》丶《媒介融合/全媒体信息编辑》的习得。
